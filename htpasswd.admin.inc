<?php
/**
 * @file
 * HTPasswd module's admin functions.
 */

/**
 * Generates administration form for the module.
 */
function htpasswd_config_form($form, &$form_state) {

  $form['htpasswd_user'] = array(
    '#type' => 'textfield',
    '#title' => t('Username'),
    '#default_value' => variable_get('htpasswd_user'),
    '#required' => TRUE,
  );

  $form['htpasswd_pass'] = array(
    '#type' => 'textfield',
    '#title' => t('Password'),
    '#default_value' => variable_get('htpasswd_pass'),
    '#required' => TRUE,
  );

  $form['htpasswd_realm'] = array(
    '#type' => 'textfield',
    '#title' => t('Realm'),
    '#deacription' => t('This message will show in the authentication window.'),
    '#default_value' => variable_get('htpasswd_realm'),
    '#required' => TRUE,
  );

  return system_settings_form($form);
}
